SETS
M 'moons'      /MoonSun,MoonWind,MoonBurn,MoonNuke/
*on alien moons day runs from 1-6 and night from 7-12
*one period of interest is 12 time simulation units
t 'time in simulation units'        /1*36/

tlast(t)    'last time';

tlast(t)$[t.val=CARD(t)]=yes;

DISPLAY tlast;

*trying importing from an Excel file for the yield table instead of using a table.
parameter gooyield(t,M) "Mapping Moons to time for yield";
*Load the Mt mapping:
$call 'csv2gdx gooyield.csv ID=gooyield Index=1 UseHeader=Y Values=2..LastCol Output=gooyield.gdx'
$gdxin gooyield.gdx
$load gooyield

*importing from an Excel file (saved as an csv) for the delivery cost table instead of using the table.
parameter deliverycost(t,M) "cost to deliver goo per unit goo (unit money per unit goo)";
$call 'csv2gdx deliverycost.csv ID=deliverycost Index=1 UseHeader=Y Values=2..LastCol Output=deliverycost.gdx'
$gdxin deliverycost.gdx
$load deliverycost

parameter gooproductioncost(M) "cost to produce goo (unit money per tonne)";
$call 'csv2gdx gooproductioncost.csv ID=gooproductioncost Index=1 UseHeader=Y Values=2..LastCol Output=gooproductioncost.gdx'
$gdxin gooproductioncost.gdx
$load gooproductioncost

parameter demandZOG(t) "demand for goo at planet ZOG (tonnes)";
$call 'csv2gdx demandZOG.csv ID=demandZOG Index=1 UseHeader=Y Values=2..LastCol Output=demandZOG.gdx'
$gdxin demandZOG.gdx
$load demandZOG

parameter landmoons(M)     "amount of goo farmland (unit land)";
$call 'csv2gdx landmoons.csv ID=landmoons Index=1 UseHeader=Y Values=2..LastCol Output=landmoons.gdx'
$gdxin landmoons.gdx
$load landmoons

parameter deltarampfix(M)      "Fixing maximum ramping amount allowed";
$call 'csv2gdx deltarampfix.csv ID=deltarampfix Index=1 UseHeader=Y Values=2..LastCol Output=deltarampfix.gdx'
$gdxin deltarampfix.gdx
$load deltarampfix

PARAMETER
*cost penalty of an overshoot
costoverperunit  Cost of an overshoot per unit goo of overshoot
*cost penalty of an undershoot
costunderperunit Cost of an undershoot per unit goo of undershoot;

costoverperunit=5;
costunderperunit=60;


POSITIVE VARIABLE
*timestored(t)               Length of time goo has been in storaage
amountstored(t)          Amount of goo stored at each time
*below must be greater than or equal to demand
totgooprod(M,t)        Goo produced from each moon;

*initialising time stored to be zero
*timestored.L(t)=0;
*setting expiry date on goo
*timestored.UP(t)=3;

*setting limit to storage capacity at each time
amountstored.UP(t)=5;
*amountstored.fx("1")=1;
amountstored.fx(tlast)=5;


*introducing ramping for nuclear fuel
VARIABLE
totcostgoo    Total cost of goo on planet ZOG
deltaramp(M,t)  Amount of ramping between times;

*if there is a value for deltarampfix enforce limit. If not don't.
deltaramp.UP(M,t)$[deltarampfix(M)]=deltarampfix(M);
deltaramp.LO(M,t)$[deltarampfix(M)]=-deltarampfix(M);

*slack variables
POSITIVE VARIABLE
undershoot(t)       Not enough goo produced in units of goo
excess(t)           Too much goo produced in units of goo
undershootpenalty   Total cost penalty undershoot
excesspenalty       Total cost penalty excess
deliverycosttot(t)  Total cost of delivering goo at each unit time to ZOG from all 4 moons
*introduced to consider when storage is cheap
costattimestep(t)        Cost of goo at a particular time
pumping(t)       Amount of goo put into storage at a given timestep
production(t)    Amount of goo taken out of storage to more cheaply satisfy demand;


EQUATIONS
Qdeliverycosttot(t)            Equation working out cost of delivery for goo each unit time
Qundershootpenalty       Equation working out cost penalty for an undershoot of goo
Qexcesspenalty           Equation working out cost penalty for an excess of goo
Qtotgooprod(M,t)        Equation working out total amount of goo produced on each moon
Qgoomeetdemand(t)  Equation forcing goo supplied to at least meet demand at each time
Qtotcostgoo      Equation working out total cost of goo
Qdeltaramp(M,t)     Equation calculating the desired ramping of nuclear
Qamountstored(t) Equation working out the maximum amount of goo that can be stored at a given time-step
*only need to enforce on these 2 because these are the equations containing t-1
QBCramp     Enforcing time boundary condition for ramping
QBCstore    Enforcing time boundary condition for storage;

*BELOW YOU ARE TRYING TO LOOP FIRST TO LAST TIME VALUE TO ENFORCE BCs
QBCramp(M,t)$[tlast(t)]..        deltaramp(M,"1")=E=totgooprod(M,"1")-totgooprod(M,t);
QBCstore(t)$[tlast(t)]..       amountstored("1")=E=0.99*amountstored(t)+0.9*pumping("1")-production("1");
Qdeltaramp(M,t)$[not t.val=1]..   deltaramp(M,t)=E=totgooprod(M,t)-totgooprod(M,t-1);
Qdeliverycosttot(t)..       deliverycosttot(t)=E=SUM(M,totgooprod(M,t)*deliverycost(t,M));
Qtotgooprod(M,t)..    totgooprod(M,t)=L=gooyield(t,M)*landmoons(M);
*adding the goo decay to the model with the multiplier in front of amountstored term
Qamountstored(t)$[not t.val=1]..       amountstored(t)=E=0.99*amountstored(t-1)+0.9*pumping(t)-production(t);
Qgoomeetdemand(t)..   sum(M,totgooprod(M,t))+production(t)+undershoot(t)=E=demandZOG(t)+pumping(t)+excess(t);
Qundershootpenalty..  undershootpenalty=E=sum(t,undershoot(t)*costunderperunit);
Qexcesspenalty..      excesspenalty=E=sum(t,excess(t)*costoverperunit);
Qtotcostgoo..    totcostgoo=E=SUM((M,t),totgooprod(M,t)*gooproductioncost(M))+sum(t,deliverycosttot(t))+undershootpenalty+excesspenalty;

*want to minimise cost of goo to our planet whilst meeting demand
MODEL ALIENSWANTGOO /ALL/;

SOLVE ALIENSWANTGOO USING DNLP MINIMIZING totcostgoo;

*outputting total goo produced to an Excel file
execute_UNLOAD 'totgooprod.gdx',totgooprod,M,t;
execute "Python bargraphprod.py";
execute 'GDXXRW.EXE totgooprod.gdx var=totgooprod';

*outputting stored goo to an Excel file
execute_UNLOAD 'amountstored.gdx', amountstored;
execute "Python bargraphstored.py";
execute 'GDXXRW.EXE amountstored.gdx var=amountstored';



