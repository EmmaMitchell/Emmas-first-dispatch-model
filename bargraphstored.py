import plotly.express as px
import pandas as pds
import plotly
import gdxpds

dict_df = gdxpds.to_dataframe('amountstored.gdx',"amountstored")
amountstoreddf=dict_df["amountstored"]
fig=px.bar(amountstoreddf,x="t",y="Level",title="Stored vs Time")
fig.update_layout(xaxis_title="Times (time units)",yaxis_title="Amount Stored (tonnes)")
fig.show()